# README #

This a multi-room chat done with JavaSpaces with IRC interface looklike. Room's names are unique and so are the user's nicks too.

### How do I get set up? ###

If you already have Java 8 working you just need to get the start-services.sh script at river directory running.

 
## Dependencies

* Java 8
* JavaSpaces
* Swing
* jini-core
* jini-ext
* jsk-lib
* outtrigger
* reggie

### How to I run? ###


1. Run start-services.sh script at river directory;
2. Run as many as you want instances of clients ;


### Who do I talk to? ###

* Repo owner or admin (aranhaqg@gmail.com)