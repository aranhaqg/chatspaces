package br.edu.ifce.ppd.client;

import net.jini.core.event.RemoteEvent;
import net.jini.core.event.RemoteEventListener;
import net.jini.export.Exporter;
import net.jini.jeri.BasicILFactory;
import net.jini.jeri.BasicJeriExporter;
import net.jini.jeri.tcp.TcpServerEndpoint;
import net.jini.space.JavaSpace;

import javax.swing.*;
import java.rmi.RemoteException;

/**
 * Created by pi on 16/12/15.
 */
class MessageEventListener implements RemoteEventListener {
    private RemoteEventListener theStub;
    private JTextArea textArea;
    private JavaSpace space;
    private String currentNick;
    private String currentRoom;

    MessageEventListener() throws RemoteException {
        Exporter myDefaultExporter =
                new BasicJeriExporter(TcpServerEndpoint.getInstance(0),new BasicILFactory(), false, true);

        theStub = (RemoteEventListener) myDefaultExporter.export(this);
    }

    RemoteEventListener getStub() {
        return theStub;
    }

    public void setNick(String currentNick) {
        this.currentNick = currentNick;
    }

    public void setRoom(String currentRoom) {
        this.currentRoom = currentRoom;
    }

    public void setSpace(JavaSpace space) {
        this.space = space;
    }

    public void setTextArea(JTextArea textArea){
        this.textArea = textArea;
    }

    public void notify(RemoteEvent anEvent) {
        try {
            String metadata = (String) (anEvent.getRegistrationObject().get());


            String to = metadata;
            System.out.println("Notify MessageEventListener metadada: "+ metadata);
            Message template = new Message();
            Message receivedMsg = null;
            template.to = to;

            do {
                receivedMsg = (Message) space.takeIfExists(template, null, 3000);
            } while (null==receivedMsg);

            if (receivedMsg.content.isEmpty()){
                //Usuário entrou na sala
                switch (receivedMsg.info){
                    case "join":
                        Utils.appendToHistory(textArea, "Usuário "+receivedMsg.from + " entou na sala.");
                        break;
                    case "leave":
                        Utils.appendToHistory(textArea, "Usuário "+receivedMsg.from + " saiu da sala.");
                        break;
                    case "disconnect":
                        Utils.appendToHistory(textArea, "Usuário "+receivedMsg.from + " desconectou.");
                        break;
                }

            }else {
                Utils.appendToHistory(textArea, receivedMsg.from, receivedMsg.content);
            }


            System.out.println("Got message event from " + receivedMsg.from + " to " + receivedMsg.to + " content " +
                    receivedMsg.content);
                    /*anEvent.getID() + ", " +
                    anEvent.getSequenceNumber() + ", " +
                    anEvent.getRegistrationObject().get());*/
        } catch (Exception anE) {
            System.out.println("Got event but couldn't display it");
            anE.printStackTrace(System.out);
        }
    }
}
