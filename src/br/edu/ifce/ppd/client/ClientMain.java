package br.edu.ifce.ppd.client;

import java.awt.EventQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import net.jini.space.JavaSpace;

public class ClientMain { 
	private JavaSpace space;

	public JavaSpace getSpace() { 
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<JavaSpace> future = executor.submit(new Task());
		
		space = null;

		// Tentando se conectar ao Space por 5 segundos,
		// caso não haja conexão é apresentada tela de falha.
		try {
			space = future.get(5, TimeUnit.SECONDS);

		} catch (TimeoutException e) {
			JFrame frame = new JFrame("ChatSpace");
			JOptionPane.showMessageDialog(frame,
					"O servico JavaSpace nao foi encontrado. Encerrando...");
			System.exit(-1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		executor.shutdownNow();
		
		return space;
	}
	
	public static void main(String[] args) {
		ClientMain cMain = new ClientMain();
		
		// Inicializando a GUI
		if(cMain.getSpace() != null) {
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					GUI gui = new GUI(cMain.getSpace());
				}
			});
		}
	}
	
	// Método que busca o Space
	static class Task implements Callable<JavaSpace> {
		@Override
		public JavaSpace call() throws Exception {
			Lookup finder = new Lookup(JavaSpace.class);
			JavaSpace space = (JavaSpace) finder.getService();
			return space;
		}
	}
}
