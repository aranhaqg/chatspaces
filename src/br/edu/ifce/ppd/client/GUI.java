package br.edu.ifce.ppd.client;

import net.jini.core.entry.UnusableEntryException;
import net.jini.core.event.EventRegistration;
import net.jini.core.lease.Lease;
import net.jini.core.transaction.TransactionException;
import net.jini.lease.LeaseRenewalManager;
import net.jini.space.JavaSpace;

import java.io.IOException;
import java.rmi.MarshalledObject;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.text.DefaultCaret;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class GUI {
	// parseCommand()
	// quit()
	// showHelp()
	// joinRoom(room)
	// listRooms()
	// listUsers(room)
	// registerNickname(nick)
	// sendMessageToRoom(room)
	// sendMessageToUser(user)
	final static int READ_TIMEOUT = 5000; //5 segundos
	final static int WRITE_TIMEOUT = 5*60*1000; 	// Mensagens são apagadas após 5 min (5*60*1000)
	final static int WRITE_NICK_TIMEOUT = 60*60*1000; //Nick não expira por 60 min

	private JFrame frame;
	private String[] helpMessage = {
			"",
			"Bem vindo ao ChatSpace. Para enviar mensagens para a sala atual, ",
			"digite a mensagem na área de texto abaixo e pressione ENTER. ",
			"Para enviar comandos, digite o comando (iniciado por /) na área ",
			"de texto abaixo e pressione ENTER. ",
			"",
			"Comandos disponíveis: ",
			"/nick <nickname> : registra seu nick¹ ",
			"/rooms : lista as salas ativas ",
			"/join <room> : entra em uma sala ativa ou cria uma nova sala ",
			"/users : lista usuários da sala atual ",
			"/pvt <user> : envia uma mensagem privada para um usuário ",
			"/quit : fecha o ChatSpace ",
			"/whois <user> : mostra informações sobre um usuário conectado",
			"/whoiam : mostra as suas informações",
			"/help : mostra esta mensagem de ajuda ",
			"",
			"¹ APENAS USUÁRIOS COM NICK PODEM ENVIAR MENSAGENS. ",
			" "
	} ;

	private JTextField textArea;
	private JTextArea messageHistory;
	private JavaSpace space;
	private String currentRoom;
	private String currentNick;
	private MessageEventListener msgListener;


	public GUI(JavaSpace space) {
		this.space = space;
		this.currentRoom = "";
		this.currentNick = "";
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(10, 10));

		textArea = new JTextField();
		textArea.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				parseMessage(textArea.getText());
				textArea.setText("");
			}});
		frame.getContentPane().add(textArea, BorderLayout.SOUTH);

		messageHistory = new JTextArea();
		messageHistory.setLineWrap(true);
		messageHistory.setEditable(false);
		DefaultCaret caret = (DefaultCaret)messageHistory.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(messageHistory);

		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		frame.setVisible(true);

		showHelp();
	}

	private void parseMessage(String message) {
		ArrayList<String> rooms = new ArrayList<String>();
		ArrayList<String> nicks = new ArrayList<String>();
		User template;
		if(message.startsWith("/")) {
			String command = message.split("\\s+")[0];

			switch (command) {
				case "/nick":
					// Nick vazio ou com espaço em branco no meio
					if(message.split("\\s+").length!=2) {
						Utils.appendToHistory(messageHistory, "Nick inválido.");
					} else {
						// Nick válido
						String newNick = message.split("\\s+")[1];
						Utils.appendToHistory(messageHistory, "Verificando disponibilidade de nick... ");
						User user = null;

						try {
							// Verificando se nick já está registrado
							template = new User();
							template.nick = newNick;

							user = (User) space.read(template, null, READ_TIMEOUT);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(user!=null){
							Utils.appendToHistory(messageHistory, "O nick "+user.nick+" já está em uso.");
						} else {
							// Nick disponível
							try {
								user = new User();
								user.nick = newNick;
								user.room = "principal";


								space.write(user, null, WRITE_NICK_TIMEOUT);
								currentNick = newNick;
								currentRoom = "principal";
								Utils.appendToHistory(messageHistory, "Nick "+currentNick+" registrado com sucesso.");
								Utils.appendToHistory(messageHistory, "Você está na sala " + currentRoom + ".");

								registerMsgEvents();

								notifyRoomUsers("join");

								} catch (IOException | TransactionException e) {
								e.printStackTrace();
							}
						}
					}
					break;
				case "/rooms":
					try {
						rooms = getRooms();

						//Verifica se o usuário continua conectado
						if(!connected()){
							break;
						}

						Utils.appendToHistory(messageHistory, "Salas disponíveis: ");
						for (String room : rooms) {
							Utils.appendToHistory(messageHistory, " - "+ room);
						}

					} catch (RemoteException | UnusableEntryException
							| TransactionException | InterruptedException e) {
						Utils.appendToHistory(messageHistory, "Não foi possível listar as salas. Tente novamente.");
					}

					break;
				case "/join":
					String newRoom = message.split("\\s+")[1];

					try {
						//Verifica se o usuário continua conectado
						if(!connected()){
							break;
						}
						//Verifica se o usuário já está na sala escolhida
						if (newRoom.equals(currentRoom)){
							Utils.appendToHistory(messageHistory, "Você já está nessa sala.");
						}else{
							//Informa usuário caso a sala seja nova
							if(roomAlreadyExists(newRoom)){
								Utils.appendToHistory(messageHistory, "A sala já existe. Mudando de sala...");
							}else{
								Utils.appendToHistory(messageHistory, "Criando a sala "+ newRoom + "...");
							}

							notifyRoomUsers("leave");

							template = new User();
							template.nick = currentNick;

							User me = (User) space.takeIfExists(template, null, READ_TIMEOUT);

							if(me!=null){
								me.room = newRoom;
								space.write(me,null, WRITE_TIMEOUT);
								currentRoom = newRoom;


								notifyRoomUsers("join");
								Utils.appendToHistory(messageHistory, "Você entrou na sala " + newRoom + ".");
							}
						}

					} catch (RemoteException | UnusableEntryException
							| TransactionException | InterruptedException e) {
						Utils.appendToHistory(messageHistory, "Não foi possível mudar de sala.");
						Utils.appendToHistory(messageHistory, "Tente novamente.");
						e.printStackTrace();
					}

					break;
				case "/users":
					try {

						//Verifica se o usuário continua conectado
						if(!connected()){
							break;
						}

						nicks = getUsers(currentRoom);

						Utils.appendToHistory(messageHistory,"Usuários na sala " + currentRoom + ":");
						for (String nick : nicks) {
							Utils.appendToHistory(messageHistory, " - "+ nick);
						}

					} catch (RemoteException | UnusableEntryException
							| TransactionException | InterruptedException e) {
						Utils.appendToHistory(messageHistory, "Não foi possível listar usuários. Tente novamente.");
					}
					break;
				case "/pvt":
					String to = message.split("\\s")[1];

					StringBuilder content = new StringBuilder();
					//Recuperando mensagem privada para usuário
					for(int i=2; i< message.split("\\s+").length; i++){
						content.append(message.split("\\s")[i]);
						content.append(" ");
					}


					System.out.println("Message :" + content.toString() + " | to: " + to);
					try{
						//Verifica se o usuário continua conectado
						if(!connected()){
							break;
						}

						//Verifica se o usário destinatário existe
						template = new User();
						template.nick = to;
						if (space.readIfExists(template,null, READ_TIMEOUT)==null){
							Utils.appendToHistory(messageHistory, "Usuário "+to+" não está conectado.");
							break;
						}

						if(content.length()>0){
							Message msg = new Message();
							msg.content = "<"+to+"> " + content.toString();
							msg.from = currentNick;
							msg.to = to;
							msg.info = "";

							space.write(msg,null,WRITE_TIMEOUT);

							Utils.appendToHistory(messageHistory, currentNick,"<"+to+"> " + content.toString());
						}

					}catch(RemoteException | UnusableEntryException	| TransactionException | InterruptedException e){
						Utils.appendToHistory(messageHistory, "Não foi possível enviar mensagem privada.");
						Utils.appendToHistory(messageHistory, "Tente novamente.");
					}


					break;
				case "/quit":
					Utils.appendToHistory(messageHistory, "Desconectando do sevidor...");
					template = new User();
					template.nick = currentNick;
					template.room = currentRoom;
					try{
						space.takeIfExists(template,null, READ_TIMEOUT);
					}catch(RemoteException | UnusableEntryException	| TransactionException | InterruptedException e) {
						Utils.appendToHistory(messageHistory, "Não foi possível desconectar do servidor. Tente novamente.");
					}
					Utils.appendToHistory(messageHistory, "Desconectado do servidor.");

					notifyRoomUsers("disconnect");

					frame.dispose();
					System.exit(0);

					break;
				case "/help":
					showHelp();
					break;
				case "/whois":
					String userNick = message.split("\\s+")[1];
					try {
						//Verifica se o usuário continua conectado
						if(!connected()){
							break;
						}

						template = new User();
						template.nick = userNick;

						User user = (User) space.read(template, null, READ_TIMEOUT);
						if(user!=null){
							Utils.appendToHistory(messageHistory, "Usuario: "+ user.nick + " | Sala: " + user.room);
						}else{
							Utils.appendToHistory(messageHistory, "Usuario " + userNick + " nao encontrado no servidor.");
						}

					} catch (Exception e) {
						Utils.appendToHistory(messageHistory, "Nao foi possivel buscar usuario.");
					}

					break;
				case "/whoiam":
					Utils.appendToHistory(messageHistory," Voce esta conectado com o nick "+ currentNick +" e esta na sala "+currentRoom +".");
					break;
				default:
					Utils.appendToHistory(messageHistory, "Comando inválido.");
					break;
			}
		} else {
			// Apenas enviando mensagem se estiver com nick registrado
			// e conectado à uma sala.

			if(currentRoom.isEmpty() || currentNick.isEmpty()) {
				Utils.appendToHistory(messageHistory, "Você precisa estar em uma sala e com um nick ");
				Utils.appendToHistory(messageHistory, "registrado para enviar mensagens.");
			} else {
				try {
					//Verifica se o usuário continua conectado
					if(connected()){

						Message msg;

						for (String toUser:getUsers(currentRoom)) {
							msg = new Message();
							msg.content = message;
							msg.from = currentNick;
							msg.info = "";
							msg.to = toUser;
							space.write(msg, null, WRITE_TIMEOUT);
						}

					}
				} catch (RemoteException |  UnusableEntryException | TransactionException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void notifyRoomUsers(String info) {
		Message msg;


		try {
			for (String toUser:getUsers(currentRoom)) {
				msg = new Message();
				msg.content = "";
				msg.from = currentNick;
				msg.to = toUser;
				msg.info = info;
                space.write(msg, null, WRITE_TIMEOUT);
            }
		} catch (UnusableEntryException | TransactionException | InterruptedException | RemoteException e) {
			e.printStackTrace();
		}
	}

	private void registerMsgEvents() {


		//Registrando eventos
		try {
            msgListener =  new MessageEventListener();


            msgListener.setSpace(space);
            msgListener.setTextArea(messageHistory);
            msgListener.setNick(currentNick);
            msgListener.setRoom(currentRoom);

            LeaseRenewalManager manager = new LeaseRenewalManager();

            Message templateMessage = new Message();
			templateMessage.to = currentNick;


            EventRegistration MsgReg = space.notify(templateMessage, null, msgListener.getStub(),
                    5*60*1000, new MarshalledObject(currentNick));

            manager.renewFor(MsgReg.getLease(), Lease.FOREVER,30000, new DebugListener());

        } catch (TransactionException | IOException e) {
            e.printStackTrace();
        }
	}



	private ArrayList<String> getRooms() throws UnusableEntryException, TransactionException,InterruptedException, RemoteException {
		//Recuperando todos os usuários existentes e salas
		ArrayList<String> rooms = new ArrayList<String>();
		ArrayList<User> users= new ArrayList<User>();
		User template = new User();

		User aux = (User) space.takeIfExists(template,null, READ_TIMEOUT);
		while(aux!=null){
			users.add(aux);
			rooms.add(aux.room);
			System.out.println("Aux getRooms " + aux.nick + " | " + aux.room);
			aux = (User) space.takeIfExists(template, null, READ_TIMEOUT);
		}

		//Removendo elementos duplicados
		System.out.println(rooms.toString());
		Set<String> hs = new HashSet<>();
		hs.addAll(rooms);
		rooms.clear();
		rooms.addAll(hs);
		System.out.println(rooms.toString());

		//Devolvendo Entries ao Space
		for (User user : users) {
			space.write(user, null, WRITE_TIMEOUT);
		}
		return rooms;
	}

	private boolean roomAlreadyExists(String newRoom) throws UnusableEntryException, TransactionException,InterruptedException, RemoteException{
		ArrayList<String> rooms = getRooms();

		for (String room : rooms ) {
			if (room.equals(newRoom)) return true;
		}

		return false;
	}

	private boolean connected() throws UnusableEntryException, TransactionException,InterruptedException, RemoteException{
		User template = new User();
		template.nick = currentNick;

		if(space.readIfExists(template, null, READ_TIMEOUT)==null){
			Utils.appendToHistory(messageHistory, "Deconectado por inatividade.");
			Utils.appendToHistory(messageHistory, "Digite /nick <nickname> para registrar-se novamente.");
			return false;
		}
		return true;
	}

	private ArrayList<String> getUsers() throws UnusableEntryException, TransactionException,InterruptedException, RemoteException {
		//Recuperando todos os usuários existentes e salas
		ArrayList<String> nicks = new ArrayList<String>();
		ArrayList<User> users= new ArrayList<User>();
		User template = new User();

		User aux = (User) space.takeIfExists(template,null, READ_TIMEOUT);
		while(aux!=null){
			users.add(aux);
			nicks.add(aux.nick);
			System.out.println("Aux getNicks " + aux.nick + " | " + aux.room);
			aux = (User) space.takeIfExists(template, null, READ_TIMEOUT);
		}

		//Removendo elementos duplicados
		System.out.println(nicks.toString());
		Set<String> hs = new HashSet<>();
		hs.addAll(nicks);
		nicks.clear();
		nicks.addAll(hs);
		System.out.println(nicks.toString());

		//Devolvendo Entries ao Space
		for (User user : users) {
			space.write(user, null, WRITE_TIMEOUT);
		}
		return nicks;
	}

	private ArrayList<String> getUsers(String room) throws UnusableEntryException, TransactionException,InterruptedException, RemoteException {
		//Recuperando todos os usuários existentes na sala definida
		ArrayList<String> nicks = new ArrayList<String>();
		ArrayList<User> users= new ArrayList<User>();
		User template = new User();
		template.room = room;

		User aux = (User) space.takeIfExists(template,null, READ_TIMEOUT);
		while(aux!=null){
			users.add(aux);
			nicks.add(aux.nick);
			System.out.println("Aux getNicks " + aux.nick + " | " + aux.room);
			aux = (User) space.takeIfExists(template, null, READ_TIMEOUT);
		}

		//Removendo elementos duplicados
		System.out.println(nicks.toString());
		Set<String> hs = new HashSet<>();
		hs.addAll(nicks);
		nicks.clear();
		nicks.addAll(hs);
		System.out.println(nicks.toString());

		//Devolvendo Entries ao Space
		for (User user : users) {
			space.write(user, null, WRITE_TIMEOUT);
		}
		return nicks;
	}

	private void showHelp() {
		for (String line : helpMessage) {
			Utils.appendToHistory(messageHistory,line);
		}
	}
}
