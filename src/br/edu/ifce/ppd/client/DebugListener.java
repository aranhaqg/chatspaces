package br.edu.ifce.ppd.client;

import net.jini.lease.LeaseListener;
import net.jini.lease.LeaseRenewalEvent;

/**
 * Created by pi on 16/12/15.
 */
public class DebugListener implements LeaseListener {
    public void notify(LeaseRenewalEvent anEvent) {
        System.err.println("Got lease renewal problem");

        System.err.println(anEvent.getException());
        System.err.println(anEvent.getExpiration());
        System.err.println(anEvent.getLease());
    }
}
