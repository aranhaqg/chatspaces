package br.edu.ifce.ppd.client;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by pi on 16/12/15.
 */
public class Utils {

    //Mensagens do servidor
    public static void appendToHistory(JTextArea messageHistory, String message) {
        messageHistory.append(getTimeStamp()+" ** "+message+"\n");
    }

    //Mensagens para a sala e bate-papo/privadas
    public static void appendToHistory(JTextArea messageHistory, String user, String message) {
        messageHistory.append(getTimeStamp()+"<"+user+"> "+message+"\n");
    }

    public static String getTimeStamp() {
        String timeStamp = new SimpleDateFormat("[HH:mm:ss] ").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
}
